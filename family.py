import pandas as pd
import numpy as np
from tabulate import tabulate

family_data = pd.read_csv('familia.csv', index_col=None, header=None, delimiter=",", na_values='_')
naDf = pd.read_csv('familia_na.csv', index_col=None, header=None, delimiter=",", na_values='_')

# naDF vem no formato B é ___ de A
# Coloca as cols do naDF na mesma ordem de df, A é __ de B
naDf[[0,2]] = naDf[[2,0]]

# Une os dataframes
#family_data = pd.concat([df, naDf], ignore_index=True, sort =False)

# 'mae' para 'mãe'
family_data[1] = family_data[1].str.replace('mae','mãe')

# Inverte as colunas de B é filho de A para A é pai de B
family_data[[0,2]] = family_data[[2,0]].where(family_data[1] == 'filho de', family_data[[0,2]].values)
family_data[1] = family_data[1].str.replace('filho de','pai')

# Um casal é A & B
family_data[1] = family_data[1].str.replace('esposo','&')
family_data[1] = family_data[1].str.replace('esposa','&')

parentes_unicos = family_data[0].unique()
dictionaryOfFamilies = {}    
for key, value in np.ndenumerate(parentes_unicos):    
    dfA = family_data.loc[family_data[0].str.contains(value)]
    dfB = family_data.loc[family_data[2].str.contains(value)]
    dictionaryOfFamilies[value] = pd.concat([dfA, dfB], ignore_index=True, sort =False)

for index, row in naDf.iterrows():
    print('#####', row[0], row[1], row[2], '#####')
    for key, df in dictionaryOfFamilies.items():
        parentA = df.loc[df[0] == row[0]].dropna()
        if not parentA.empty:
            print(tabulate(parentA, headers='keys', tablefmt='psql'))        
        parentB = df.loc[df[2] == row[2]].dropna()
        if not parentB.empty:
            print(tabulate(parentB, headers='keys', tablefmt='psql'))
        
        
